package ru.tsc.kyurinova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.model.Project;

public interface IProjectService extends IOwnerService<Project> {

    void create(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Project findByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Project removeByName(@Nullable String userId, @Nullable String name);

    @Nullable
    Project updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @NotNull String description);

    @NotNull
    Project updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @NotNull String description);

    @Nullable
    Project startById(@Nullable String userId, @Nullable String id);

    @NotNull
    Project startByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Project startByName(@Nullable String userId, @Nullable String name);

    @Nullable
    Project finishById(@Nullable String userId, @Nullable String id);

    @NotNull
    Project finishByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Project finishByName(@Nullable String userId, @Nullable String name);

    @Nullable
    Project changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Project changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    Project changeStatusByName(@Nullable String userId, @Nullable String name, @Nullable Status status);

}
