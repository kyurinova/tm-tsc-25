package ru.tsc.kyurinova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kyurinova.tm.command.AbstractCommand;

public class DeveloperInfoShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "about";
    }

    @NotNull
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String description() {
        return "Display developer info...";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: " + serviceLocator.getPropertyService().getDeveloperName());
        System.out.println("E-MAIL: " + serviceLocator.getPropertyService().getDeveloperEmail());
    }
}
