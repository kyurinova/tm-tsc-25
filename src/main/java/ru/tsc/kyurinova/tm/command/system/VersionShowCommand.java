package ru.tsc.kyurinova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kyurinova.tm.command.AbstractCommand;

public class VersionShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "version";
    }

    @NotNull
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String description() {
        return "Display program version...";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println(serviceLocator.getPropertyService().getApplicationVersion());
    }
}
