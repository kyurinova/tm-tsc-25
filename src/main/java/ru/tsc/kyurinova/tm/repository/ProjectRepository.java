package ru.tsc.kyurinova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.repository.IProjectRepository;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.model.Project;

import java.util.Optional;

public final class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public Project findByName(@NotNull final String userId, @NotNull final String name) {
        return entities.stream()
                .filter(p -> p.getId().equals(name))
                .filter(p -> p.getUserId().equals(userId))
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);
    }

    @NotNull
    @Override
    public Project removeByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Project project = findByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        entities.remove(project);
        return project;
    }

    @Nullable
    @Override
    public Project startById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @NotNull
    @Override
    public Project startByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final Project project = findByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @NotNull
    @Override
    public Project startByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Project project = findByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Nullable
    @Override
    public Project finishById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @NotNull
    @Override
    public Project finishByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final Project project = findByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @NotNull
    @Override
    public Project finishByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Project project = findByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Nullable
    @Override
    public Project changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        @Nullable final Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @NotNull
    @Override
    public Project changeStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status) {
        @Nullable final Project project = findByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @NotNull
    @Override
    public Project changeStatusByName(@NotNull final String userId, @NotNull final String name, @NotNull final Status status) {
        @Nullable final Project project = findByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

}
