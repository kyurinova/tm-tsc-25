package ru.tsc.kyurinova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.repository.IOwnerRepository;
import ru.tsc.kyurinova.tm.model.AbstractOwnerEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E>
        implements IOwnerRepository<E> {

    @Override
    public void add(@NotNull final String userId, @NotNull final E entity) {
        if (userId.equals(entity.getUserId())) {
            entities.add(entity);
        }
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final E entity) {
        if (userId.equals(entity.getUserId()))
            entities.remove(entity);
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId) {
        return entities.stream()
                .filter(e -> e.getUserId().equals(userId))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId, @NotNull final Comparator<E> comparator) {
        return entities.stream()
                .filter(e -> e.getUserId().equals(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<E> listOfProjects = findAll(userId);
        entities.removeAll(listOfProjects);
    }

    @Nullable
    @Override
    public E findById(@NotNull final String userId, @NotNull final String id) {
        return entities.stream()
                .filter(e -> e.getId().equals(id))
                .filter(e -> e.getUserId().equals(userId))
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public E findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final List<E> listOfEntities = findAll(userId);
        return listOfEntities.get(index);
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final Optional<E> entity = Optional.ofNullable(findById(userId, id));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

    @Nullable
    @Override
    public E removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Optional<E> entity = Optional.ofNullable(findByIndex(userId, index));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final Optional<E> entity = Optional.ofNullable(findById(userId, id));
        return entity != null;
    }

    @Override
    public boolean existsByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Optional<E> entity = Optional.ofNullable(findByIndex(userId, index));
        return entity != null;
    }

}
